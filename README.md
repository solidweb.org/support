# Support

> Support for [solidweb.org](https://solidweb.org/) pod server

This repository is a work in progress.  

Currently, the aims are:

- **[Knowledge base](https://www.w3.org/community/solid/wiki/Main_Page):** Information about the project
- **[New issues](https://gitlab.com/solidweb.org/support/-/issues/new):** Add a support issue
- **[History](https://gitlab.com/solidweb.org/support/issues):** Track what was done and when

[**File an issue →**](https://gitlab.com/solidweb.org/support/-/issues/new)

Issues raised here should be of a short lived support nature.  Maybe a 'proposals' category for longer lived issues would make sense.
